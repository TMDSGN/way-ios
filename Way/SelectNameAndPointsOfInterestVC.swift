//
//  SelectNameAndPointsOfInterestVC.swift
//  Way
//
//  Created by Josef Antoni on 29.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class SelectNameAndPointsOfInterestVC: HamburgerVC {
    
    let name = ["Charles bridge", "Dancing house", "Prague castle"]
    let image = ["CharlesBridge", "DancingHouse", "PragueCastle"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellRegister()
        redBtn.layer.cornerRadius = redBtn.frame.height/2
    }
    
    fileprivate func customCellRegister(){
        let nib = UINib(nibName: "PointOfInterestCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PointOfInterest")
        tableView.separatorStyle = .none
    }
    
    @IBOutlet var redBtn: UIButton!
    @IBOutlet var tableView: UITableView!
}

extension SelectNameAndPointsOfInterestVC {
    
    @IBAction func openProfileBtn(_ sender: Any) {
        showProfile()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        returnToPrevVC()
    }
}

extension SelectNameAndPointsOfInterestVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "PointOfInterest") as! PointOfInterestVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.pointOfInterestName.text = name[indexPath.row]
        cell.pointIfInterestImage.image = UIImage(named: image[indexPath.row])
        
        cell.setPickedIcon()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor : newValue!])
        }
    }
}
