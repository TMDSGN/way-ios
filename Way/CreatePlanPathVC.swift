//
//  CreatePlanPathVC.swift
//  Way
//
//  Created by Josef Antoni on 31.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CreatePlanPathVC: HamburgerVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        self.createPlanBtn.layer.cornerRadius = self.createPlanBtn.frame.height/2
        self.exploreMapBtn.layer.cornerRadius = self.exploreMapBtn.frame.height/2
    }
    
    @IBOutlet var createPlanBtn: UIButton!
    @IBOutlet var exploreMapBtn: UIButton!
}

extension CreatePlanPathVC {
    
    @IBAction func openProfileBtn(_ sender: Any) {
        showProfile()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        returnToPrevVC()
    }
    
    @IBAction func createTourPlanBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectNameAndPointsOfInterestVC") as! SelectNameAndPointsOfInterestVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func openMapBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "MapContainerVC") as! MapContainerVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
}
