//
//  CitySelectionVC.swift
//  Way
//
//  Created by Josef Antoni on 22.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CitySelectionVC: UIViewController {
    
    var cityName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.cityNameLabel.text = cityName
    }
    
    @IBOutlet weak var cityNameLabel: UILabel!
}
