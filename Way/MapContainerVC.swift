//
//  MapContainerVC.swift
//  Way
//
//  Created by Josef Antoni on 29.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Mapbox

class MapContainerVC: HamburgerVC {
    
    var locationManager: CLLocationManager = CLLocationManager()
    var locValue:CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        setupLocationManager()
        mapView.delegate = self
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.attributionButton.alpha = 0
        mapView.showsUserLocation = true
        showAnnotation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mapView.setCenter(CLLocationCoordinate2D(latitude: 50.065155553050815, longitude: 14.442566383642356), zoomLevel: 10, animated: false)
    }
    
    fileprivate func showAnnotation(){
        let hello = MGLPointAnnotation()
        hello.coordinate = CLLocationCoordinate2D(latitude: 50.075155553050815, longitude: 14.442566383642356)
        hello.title = "Hello world!"
        hello.subtitle = "Welcome to my marker"
        
        // Add marker `hello` to the map.
        mapView.addAnnotation(hello)
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBOutlet var mapView: MGLMapView!
}

extension MapContainerVC: MGLMapViewDelegate {
    
    // Use the default marker. See also: our view annotation or custom marker examples.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        return nil
    }
    
    // Allow callout view to appear when an annotation is tapped.
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
}


extension MapContainerVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
        print(locValue)
    }
    
}
