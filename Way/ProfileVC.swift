//
//  ProfileVC.swift
//  Way
//
//  Created by Josef Antoni on 28.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class ProfileVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgIcon.layer.cornerRadius = imgIcon.frame.height/2
        imgIcon.clipsToBounds = true
    }
    
    @IBOutlet var imgIcon: UIImageView!
}

extension ProfileVC {
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
