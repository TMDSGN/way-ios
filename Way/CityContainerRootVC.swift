//
//  CityContainerRootVC.swift
//  Way
//
//  Created by Josef Antoni on 22.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import EMPageViewController

class CityContainerRootVC: HamburgerVC {
    
    var pageViewController: EMPageViewController?
    
    var greetings: [String] = ["Praha", "Brno", "Ostrava"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.redBtn.layer.cornerRadius = self.redBtn.frame.height/2

        let pageViewController = EMPageViewController()
        
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
        
        self.addChildViewController(pageViewController)
        self.view.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        
        self.pageViewController = pageViewController
    }
    
    func viewController(at index: Int) -> CitySelectionVC? {
        if (self.greetings.count == 0) || (index < 0) || (index >= self.greetings.count) {
            return nil
        }
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "CitySelectionVC") as! CitySelectionVC
        viewController.cityName = self.greetings[index]
        return viewController
    }
    
    func index(of viewController: CitySelectionVC) -> Int? {
        if let greeting: String = viewController.cityName {
            return self.greetings.index(of: greeting)
        } else {
            return nil
        }
    }
    
    @IBOutlet var redBtn: UIButton!
}

extension CityContainerRootVC {

    @IBAction func exloreCityBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePlanPathVC") as! CreatePlanPathVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func openProfileBtn(_ sender: Any) {
        showProfile()
    }

    @IBAction func backBtn(_ sender: Any) {
        returnToPrevVC()
    }
}

extension CityContainerRootVC: EMPageViewControllerDataSource, EMPageViewControllerDelegate {

    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! CitySelectionVC) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! CitySelectionVC) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            return afterViewController
        } else {
            return nil
        }
    }
}
