//
//  PointOfInterestVC.swift
//  Way
//
//  Created by Josef Antoni on 29.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class PointOfInterestVC: UITableViewCell {

    var picked = false
    
    override func awakeFromNib() {
        self.pointIfInterestImage.layer.cornerRadius = self.pointIfInterestImage.frame.height/2
    }
    
    func setPickedIcon(){
        if picked {
            pickerIcon.image = UIImage(named: "Cancel")
        } else {
            pickerIcon.image = UIImage(named: "Plus")
        }
    }
    
    @IBAction func pickBtn(_ sender: Any) {
        picked = !picked
        setPickedIcon()
    }
    
    @IBOutlet var pointIfInterestImage: UIImageView!
    @IBOutlet var pointOfInterestName: UILabel!
    @IBOutlet var pickerIcon: UIImageView!
}
