//
//  LoginVC.swift
//  Way
//
//  Created by Josef Antoni on 31.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class LoginVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    fileprivate func proceedToNextView(){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCityVC") as! SelectCityVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
}

extension LoginVC {
    
    @IBAction func facebookBtn(_ sender: Any) {
        proceedToNextView()
    }
    
    @IBAction func instagramBtn(_ sender: Any) {
        proceedToNextView()
    }
}
