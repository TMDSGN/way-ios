//
//  SelectCityVC.swift
//  Way
//
//  Created by Josef Antoni on 31.07.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class SelectCityVC: HamburgerVC {
    
    fileprivate let _dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        self.selectCityBtn.layer.cornerRadius = self.selectCityBtn.frame.height/2
        self.continueBtn.layer.cornerRadius = self.continueBtn.frame.height/2
        self.selectCityBtn.backgroundColor = UIColor(red: 42.0/255, green: 46.0/255, blue: 61.0/255, alpha: 1)
        self._dropDown.dataSource = ["Germany", "Czech", "Austria"]
        self._dropDown.anchorView = self.selectCityBtn
        self._dropDown.direction = .bottom
        self._dropDown.bottomOffset = CGPoint(x: 0, y: self.selectCityBtn.bounds.height)
        self._dropDown.backgroundColor = UIColor(red: 42.0/255, green: 46.0/255, blue: 61.0/255, alpha: 1)
        self._dropDown.textFont = UIFont.systemFont(ofSize: 14)
        self._dropDown.textColor = .white
        self._dropDown.cornerRadius = 23
        self._dropDown.shadowColor = .clear
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    @IBOutlet var selectCityBtn: UIView!
    @IBOutlet var continueBtn: UIButton!
}

extension SelectCityVC {

    @IBAction func selectCityBtn(_ sender: Any) {
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "CityContainerRootVC") as! CityContainerRootVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    @IBAction func openProfileBtn(_ sender: Any) {
        showProfile()
    }
    
    @IBAction func dropDownBtn(_ sender: Any) {
        if _dropDown.isHidden {
            self._dropDown.show()
        }
        self._dropDown.selectionAction = { [unowned self] (index, item) in
            
        }
    }
}
