//
//  HamburgerVC.swift
//  Way
//
//  Created by Josef Antoni on 28.08.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class HamburgerVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showProfile(){
        let modal = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let navigationController = UINavigationController(rootViewController: modal)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func returnToPrevVC(){
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
}
